package br.edu.up.progressbar;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

  ProgressBar progressBar;
  SeekBar seekBar;
  TextView tvSeek;
  TextView tvProgress;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    progressBar = (ProgressBar) findViewById(R.id.progressBar);
    seekBar = (SeekBar) findViewById(R.id.seekBar);
    seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        tvSeek.setText("Posição: " + i);
        progressoSeekBar = i;
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {
        //ocorre quando inicia o movimento da seekbar;
      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {
        //ocorre quando terminar no movimento da seekbar;
      }
    });

    tvSeek = (TextView) findViewById(R.id.txtSeekBar);
    tvProgress = (TextView) findViewById(R.id.txtProgressBar);

  }

  public void processarSeekBar(View v){
    AtualizadorDaSeekBar processo = new AtualizadorDaSeekBar();
    processo.start();
  }

  public void processarProgress(View v){
    AtualizadorDaProgressBar processo = new AtualizadorDaProgressBar();
    processo.start();
  }

  int progressoSeekBar = 0;

  int[] musicas = {
      R.raw.anitta_bang,
      R.raw.jota_quest_na_moral,
      R.raw.pink_floyd_another_brick_in_the_wall_p2,
      R.raw.sambo_sunday_bloody_sunday,
      R.raw.talking_heads_psycho_killer,
      R.raw.the_doors_light_my_fire,
      R.raw.zeca_baleiro_disritmia
  };

  MediaPlayer player;
  public void tocar(View v){
    player = null;

  }

  class AtualizadorDaSeekBar extends Thread {
    @Override
    public void run() {
      //faz alguma coisa
      while (progressoSeekBar < 100){
        progressoSeekBar++;
        seekBar.setProgress(progressoSeekBar);
        //tvSeek.setText("Posição: " + progressoSeekBar);
        Log.d("PROGRESS", "Valor: " + progressoSeekBar);
        try {
          Thread.sleep(200); //faz um pausa por 200 milisegundos.
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  int progressoProgressBar = 0;

  class AtualizadorDaProgressBar extends Thread {
    @Override
    public void run() {
      //faz alguma coisa
      while (progressoProgressBar < 100){
        progressoProgressBar++;
        progressBar.setProgress(progressoProgressBar);
        //tvProgress.setText("Posição: " + progressBar);
        Log.d("PROGRESS", "Valor: " + progressoProgressBar);
        try {
          Thread.sleep(200); //faz um pausa por 200 milisegundos.
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
